import sys


def main():
    if len(sys.argv) != 5:
        sys.stderr.write("Incorrect number of arguments\n")
        return
    else:
        result_file = sys.argv[1]
        hunters_file = sys.argv[2]
        nonhunters_file = sys.argv[3]
        number_of_features = int(sys.argv[4])
        hunters_list = list()
        nonhunters_list = list()
        gathered_features = dict()
        order_list = list()
        with open(hunters_file) as hunters:
            for hunter in hunters:
                hunters_list.append(hunter.rstrip())

        with open(nonhunters_file) as nonhunters:
            for nonhunter in nonhunters:
                nonhunters_list.append(nonhunter.rstrip())
        
        write_feature_names = True
        with open(result_file) as rf:
            for result in rf:
                splitted = result.split()
                if write_feature_names:
                    with open('featurenames.txt', 'w') as nf:
                        feature_name_counter = 0
                        while feature_name_counter != number_of_features: 
                            nf.write('{0} '.format(splitted[feature_name_counter].split(':')[0]))
                            feature_name_counter+=1
                        nf.write('\n')
                        write_feature_names = False
                counter = 0
                features_list = list()
                while counter != number_of_features:
                    feature_value = float(splitted[counter].split(':')[1])
                    features_list.append(feature_value)
                    counter+=1
                twitter_name_or_id = splitted[number_of_features].split(':')[1]
                if twitter_name_or_id != 'None':
                    gathered_features[twitter_name_or_id] = features_list
                    order_list.append(twitter_name_or_id)
                else:
                    twitter_name_or_id = splitted[number_of_features+1].split(':')[1]
                    if twitter_name_or_id != 'None':
                        gathered_features[twitter_name_or_id] = features_list
                        order_list.append(twitter_name_or_id)

        with open('features.txt', 'w') as feature_file:
            with open('actualvalue.txt', 'w') as actual_file:
                for twitter_name_or_id in order_list:
                    if twitter_name_or_id in hunters_list:
                        actual_file.write('{0}\n'.format(1))
                    else:
                        actual_file.write('{0}\n'.format(0))
                    for feature in gathered_features[twitter_name_or_id]:
                        feature_file.write('{0} '.format(feature))
                    feature_file.write('\n')

main()

