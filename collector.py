import twitter
import appInfo
import sys
import time
import datetime

class CollectError(Exception):
    pass

class Collector(object):
   
    api = twitter.Api(consumer_secret = appInfo.CONSUMER_SECRET,
                  consumer_key = appInfo.CONSUMER_KEY,
                  access_token_key = appInfo.ACCESS_TOKEN_KEY,
                  access_token_secret = appInfo.ACCESS_TOKEN_SECRET)

    def ReviewLimit(self, resource):
        try:
            if self.api.GetSleepTime(resource) > 0:
                sys.stderr.write("Sleeping for 905 secs for resource: {0}\n".format(resource))
                time.sleep(905)
        except twitter.error.TwitterError:
            sys.stderr.write("Cannot review limit right now. Will sleep for 905 secs\n")
            time.sleep(905)

    def __init__(self, twitter_id = None, twitter_name = None, status_retrieved = 200, search_retrieved = 100):
        if twitter_id is None and twitter_name is None:
            raise CollectError("Must specify either a twitter ID or twitter name\n")

        sys.stderr.write("Creating a Collector object with twitter_id:{0} and twitter_name:{1}\n".format(twitter_id, twitter_name))
        self.twitter_id = twitter_id
        self.twitter_name = twitter_name
        self.user = None
        self.statuses = None
        self.user_search_result = None
        self.all_features = dict()
        if status_retrieved < 200:
            self.status_retrieved = 200
        else:
            if status_retrieved > 800:
                self.status_retrieved = 800
            else:
                self.status_retrieved = status_retrieved
        self.status_iteration_limit = self.status_retrieved/200

        if search_retrieved < 100:
            self.search_retrieved = 100
        else:
            if search_retrieved > 500:
                self.search_retrieved = 500
            else:
                self.search_retrieved = search_retrieved
        self.search_iteration_limit = self.search_retrieved/100
        #features
        self.two_times_following = 0
        self.follower_less_200 = 0
        self.total_tweets_less_2000 = 0
        self.created_less_6_months = 0
        self.average_posting_time_in_hours = 0.0
        self.hashtag_spamming_in_one_day = 0
        self.mention_post_ratio = 0.0
        self.unique_mention_ratio = 0.0
        self.unique_mentionner_ratio = 0.0

    def SetUser(self):
        self.ReviewLimit('/users/show/:id')
        try:
            self.user = self.api.GetUser(user_id = self.twitter_id, screen_name = self.twitter_name)
        except twitter.error.TwitterError as exc:
            raise CollectError("An error occurred while setting user: {0} for twitter name: {1} and twitter ID: {2}".format(exc, self.twitter_name, self.twitter_id))

    def SetStatuses(self):
        max_id = 0
        try:
            for x in xrange(0,self.status_iteration_limit):
                self.ReviewLimit('/statuses/user_timeline')
                if x == 0:
                    self.statuses = self.api.GetUserTimeline(count = 200, user_id = self.twitter_id, screen_name = self.twitter_name)
                    if not self.statuses:
                        break
                    max_id = self.statuses[-1].GetId() - 1
                else:
                    if x == self.status_iteration_limit - 1:
                        get_new_statuses = self.api.GetUserTimeline(count = self.status_retrieved % 200, user_id = self.twitter_id, screen_name = self.twitter_name, max_id = max_id)
                    else:
                        get_new_statuses = self.api.GetUserTimeline(count = 200, user_id = self.twitter_id, screen_name = self.twitter_name, max_id = max_id)
                    if not get_new_statuses:
                        break
                    self.statuses+= get_new_statuses
                    max_id = self.statuses[-1].GetId() - 1
        except twitter.error.TwitterError as exc:
            raise CollectError("An error occurred while setting statuses: {0} for twitter name: {1} and twitter ID: {2}".format(exc,self.twitter_name, self.twitter_id))

    def SetUserSearch(self):
        max_id = 0
        search_term = '@'+self.user.GetScreenName()
        try:
            for x in xrange(0,self.search_iteration_limit):
                self.ReviewLimit('/search/tweets')
                if x == 0:
                    self.user_search_result = self.api.GetSearch(term = search_term, count = 100)
                    if self.user_search_result: 
                        max_id = self.user_search_result[-1].GetId() - 1
                    else:
                        self.user_search_result = list()
                        break
                else:
                    if x == self.search_iteration_limit - 1:
                        get_new_searches = self.api.GetSearch(term = search_term, count = self.search_retrieved % 100, max_id = max_id)
                    else:
                        get_new_searches = self.api.GetSearch(term = search_term, count = 100, max_id = max_id)
                    if not get_new_searches:
                        break
                    self.user_search_result+= get_new_searches
                    max_id = self.user_search_result[-1].GetId() - 1
        except twitter.error.TwitterError as exc:
            raise CollectError("An error occurred while setting search result: {0} for twitter name: {1} and twitter ID: {2}".format(exc,self.twitter_name, self.twitter_id))

    def ReviewUser(self):
        if self.user is None:
            raise CollectError("User has not been set, cannot review")
        
        followers_count = self.user.GetFollowersCount()
        if followers_count:
            following_follower_ratio = float(self.user.GetFriendsCount()) / followers_count
            if following_follower_ratio > 2:
                self.two_times_following = 1
        else:
            self.two_times_following = 1
        if followers_count < 200:
            self.follower_less_200 = 1
        if self.user.GetStatusesCount() < 2000:
            self.total_tweets_less_2000 = 1
        created_date = datetime.datetime.strptime(self.user.GetCreatedAt(), '%a %b %d %H:%M:%S +0000 %Y')
        current_date = datetime.datetime.today()
        timedelta = current_date - created_date
        if timedelta.days < 180:
            self.created_less_6_months = 1
        
        self.all_features['FollowingTwoTimesMore'] = self.two_times_following
        self.all_features['FollowerLess200'] = self.follower_less_200
        self.all_features['CreatedLess6Months'] = self.created_less_6_months
        self.all_features['TotalTweetsLess2000'] = self.total_tweets_less_2000

    def ReviewStatuses(self):
        if self.statuses is None:
            raise CollectError("Statuses has not been, cannot review")
        
        last_seen = None
        total_time_in_hours = 0
        tracked_hashtag = dict()
        total_posts_with_mentions = 0.0
        total_mentioned = 0.0
        unique_mention_list = list()
        for status in self.statuses:
            if not last_seen:
                last_seen = datetime.datetime.strptime(status.GetCreatedAt(), '%a %b %d %H:%M:%S +0000 %Y')
            else:
                currently_seen = datetime.datetime.strptime(status.GetCreatedAt(), '%a %b %d %H:%M:%S +0000 %Y')
                timedelta_avg_time = last_seen - currently_seen
                total_time_in_hours+= timedelta_avg_time.seconds/60.0
                last_seen = currently_seen
            
            if not self.hashtag_spamming_in_one_day:
                for hashtag in status.hashtags:
                    if hashtag.text in tracked_hashtag:
                        timedelta_hashtag = tracked_hashtag[hashtag.text][0]-datetime.datetime.strptime(status.GetCreatedAt(), '%a %b %d %H:%M:%S +0000 %Y')
                        if timedelta_hashtag.days == 0:
                            if tracked_hashtag[hashtag.text][1]+1 > 3:
                                self.hashtag_spamming_in_one_day = 1
                                break
                            else:
                                tracked_hashtag[hashtag.text][1]+= 1
                        else:
                            tracked_hashtag[hashtag.text] = [datetime.datetime.strptime(status.GetCreatedAt(), '%a %b %d %H:%M:%S +0000 %Y'),1]
                    else:
                        tracked_hashtag[hashtag.text] = [datetime.datetime.strptime(status.GetCreatedAt(), '%a %b %d %H:%M:%S +0000 %Y'),1] 
            
            if status.user_mentions:
                total_posts_with_mentions+= 1.0
                for mentioned in status.user_mentions:
                    if mentioned not in unique_mention_list:
                        unique_mention_list.append(mentioned)
                    total_mentioned+= 1.0

        if self.statuses:
            self.mention_post_ratio = total_posts_with_mentions/len(self.statuses)
        if total_mentioned:
            self.unique_mention_ratio = len(unique_mention_list)/total_mentioned
        if (len(self.statuses) -1.0) > 0:
            self.average_posting_time_in_hours = total_time_in_hours/(len(self.statuses)-1.0)
        
        self.all_features['AverageHourlyPostingRate'] = self.average_posting_time_in_hours
        self.all_features['HashtagSpammingInOneDay'] = self.hashtag_spamming_in_one_day
        self.all_features['MentionPostRatio'] = self.mention_post_ratio
        self.all_features['UniqueMentionRatio'] = self.unique_mention_ratio

    def ReviewUserSearch(self):
        if self.user_search_result is None:
            raise CollectError("Search result for user has not been set, cannot review")
        unique_mentionner_list = list()
        total_mentionner = 0.0
        user_searched = self.user.GetScreenName()
        for search_result in self.user_search_result:
            searcher_name = search_result.GetUser().GetScreenName()
            for mentioned in search_result.user_mentions:
                if mentioned.GetScreenName() == user_searched:
                    total_mentionner+= 1.0
                    if searcher_name not in unique_mentionner_list:
                        unique_mentionner_list.append(searcher_name)
                    break
        print total_mentionner
        if total_mentionner:
            self.unique_mentionner_ratio = len(unique_mentionner_list)/total_mentionner
        
        self.all_features['UniqueMentionnerRatio'] = self.unique_mentionner_ratio

    def __str__(self):
        res = ''
        first = True
        for feature in self.all_features:
            if first:
                res+= '{}:{}'.format(feature,self.all_features[feature])    
                first = False
            else:
                res+= ' {}:{}'.format(feature,self.all_features[feature])
        return res+' TwitterName:{} TwitterID:{}'.format(self.twitter_name, self.twitter_id)


    def RunCollector(self):
        self.SetUser()
        self.SetStatuses()
        self.SetUserSearch()
        self.ReviewUser()
        self.ReviewStatuses()
        self.ReviewUserSearch()

    def FeatureScores(self):
        res = ''
        first = True
        for feature in self.all_features:
            if first:
                res+= '{}'.format(self.all_features[feature])
                first = False
            else:
                res+= ' {}'.format(self.all_features[feature])

    def FeatureNames(self):
        res = ''
        first = True
        for feature in self.all_features:
            if first:
                res+= feature
                first = False
            else:
                res+= ' '+feature
        return res
