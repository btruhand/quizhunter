QUIZHUNTER
==========

A module to find out certain features of Twitter users used to determine later on whether or not they are quizhunters. By default and minimally the module will inspect the last 200 statuses made by the Twitter user and do a search of the Twitter user up to a minimal of 100 search results. This can be extended to at most 800 statuses and a most 500 search results (by assigning a value to the status\_retrieved and search\_retrieved parameters respectively when creating the object). Either a Twitter ID or Twitter name must be given (they must be correct i.e the ID is the ID of the Twitter user with that Twitter name)

Usage
-----

An example is provided in collectdata.py. Invoke **python collectdata.py nonhunters.txt hunters.txt** to see the result which would be saved to
**resultcollecting.txt**

Modules needed
--------------

Python-Twitter by Bear is needed. For installation of the module please go to [here](https://github.com/bear/python-twitter)

Important Notes
---------------

Do not delete the file **appInfo.py** since it is required in order for the module to work

Features Gathered
-----------------

* Whether the Twitter user is following users at a rate of at least two times more than they are being followed
* Whether the number of followers the Twitter user have is below 200
* Whether the number of tweets the user has made is below 2000
* Whether the account was made in the last 6 months
* The Twitter user's average hourly posting rate i.e how long does it take for the user to post a new status in hours
* Whether they are spamming any hashtag three times in a row in one single day
* The ratio of statuses that contain a mention
* The ratio of the number of users mentioned by the Twitter user against the total number of mentions made
* The ratio of the number of users that mentioned the Twitter user against the total number of mentions that the user received

Important Functions and Files
-------------------

* Generally a you will only need to invoke RunCollector()
* A \_\_str\_\_ method is provided to give a formatted output of the object (used with *str()* or *print*) which includes the features' name, features' score and the Twitter name and ID of the Twitter user inspected
* FeatureScores() will return a single line of all the feature scores delimited by single space
* FeatureNames will return a single line of all the feature names delimited by single space
* **extractor.py** is needed to extract and create individual files for the feature names, feature values, and observed value based on gathered result in resultcollecting.txt. They are saved in the files named featurenames.txt features.txt and actualvalues.txt respectively once extractor.py is invoked

Important Notes
---------------

* hunters.txt contains the Twitter users that are quiz hunters
* nonhunters.txt contains the Twitter users that are non quiz hunters
* resultcollecting.txt contains the search result and the feature scores of 1200 of the Twitter accounts specified in hunters.txt and nonhunters.txt, roughly 600 from each
* extractor.py should be invoked in the following format: __python extractor.py *search result file* *hunters file* *nonhunters file* *number of features* thus it should currently be invoked as follows: python extractor.py *resultcollecting.txt* *hunters.txt* *nonhunters.txt* *9*__
