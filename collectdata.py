import collector
import sys
import os

def main():
    directory_path = os.path.dirname(os.path.realpath(__file__))
    limit = 600
    if len(sys.argv) == 3:
        screen_name_hunters = list()
        screen_name_nonhunters = list()
        with open(directory_path+'/'+sys.argv[1]) as f:
            for screen_name in f:
                screen_name_nonhunters.append(screen_name.rstrip())
        with open(directory_path+'/'+sys.argv[2]) as f:
            for screen_name in f:
                screen_name_hunters.append(screen_name.rstrip())
        
        counter_hunters = 0
        counter_nonhunters = 0
        with open(directory_path+'/resultcollecting.txt', 'w') as f:
            for screen_name in screen_name_nonhunters:
                try:
                    collect_quizhunter_features = collector.Collector(twitter_name = screen_name, status_retrieved = 600, search_retrieved = 500)
                    collect_quizhunter_features.RunCollector()
                    f.write("{0} \n".format(str(collect_quizhunter_features)))
                    counter_nonhunters+= 1
                    if counter_nonhunters == limit:
                        break
                except collector.CollectError as exc:
                    sys.stderr.write("{0}\n".format(str(exc)))
            
            for screen_name in screen_name_hunters:
                try:
                    collect_quizhunter_features = collector.Collector(twitter_name = screen_name, status_retrieved = 600, search_retrieved = 500)
                    collect_quizhunter_features.RunCollector()
                    f.write("{0} \n".format(str(collect_quizhunter_features)))
                    counter_hunters+= 1
                    if counter_hunters == limit:
                        break
                except collector.CollectError as exc:
                    sys.stderr.write("{0}\n".format(str(exc)))
    else:
        sys.stderr.write('Incorrect number of arguments\n')
        return

main()
